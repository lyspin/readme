# 📖 Lysanne's README

:wave: Hello! I'm Lysanne. A few things about me:

* I joined GitLab 🦊 in August 2023, just before [GitLab 16.3](https://about.gitlab.com/releases/2023/08/22/gitlab-16-3-released/) was released.
* I live in [Ottawa, Ontario 🇨🇦](https://en.wikipedia.org/wiki/Ottawa) with my partner, our tuxedo cat named Tails, and our french bulldog named Finn.
* I like to read 📚. Check out my [Goodreads profile](https://www.goodreads.com/user/show/24739961-lysanne) to see what I'm reading.
* I enjoy collecting and listening to vinyl records 🎶. Check out my [Discogs profile](https://www.discogs.com/user/pints/collection?header=1&page=1) to see my collection.

## Communication style

- I generally keep Slack and email open, but I might not reply instantly.
- I will send you an invite to a Zoom call if I need you to explain things to me.
- I appreciate ongoing feedback on things I can improve and things I am doing well.

## Working style

- I benefit from tasks being written with as much context as possible.
- I prefer to deep-dive into an issue so I can get a good understanding of the topic at hand.
- I tend to need time to process information before providing answers.
- I prefer to have information or questions prior to meetings or discussions.

## Availability

* My timezone is [ET](https://time.is/Ottawa#:~:text=Time%20zone%20info%20for%20Ottawa&text=Switched%20to%20UTC%20%2D4%20%2F%20Eastern,to%2003%3A00AM%20local%20time.). I generally work from ~10:00 to ~18:30, and do my best work in the afternoon.
    * I'm not very productive before 10 am. I usually do not accept meetings late-nights or early mornings.
    * I prefer to have coffee chats in the afternoons.
    * If you see a good time on my calendar during my working hours, please schedule a meeting (no need to ask first).
    * If you need to reschedule a meeting, go ahead!
* I do not have Slack or work email setup on my phone so I will not respond to pings unless I am at my work laptop.

### How to reach me

- **GitLab**: Please @ me directly in comments, and assign issues and MRs to me.
- **Slack**: DM or @ me. I prefer Slack for most short interactions and questions.
- **Email**: I check my emails multiple times a day.
- **Zoom**: Ideal for conversations requiring back-and-forth in-depth dialogue.

<a href="https://gitlab.enterprise.slack.com/team/U05MK1L1ZB8"><img align="left" src="https://img.shields.io/badge/Slack-4A154B?&style=for-the-badge&logo=Slack&logoColor=white" /></a>
<a href="mailto:lpinto@gitlab.com"><img align="left" src="https://img.shields.io/badge/Email-EA4335?&style=for-the-badge&logo=Gmail&logoColor=white" /></a>
<a href="https://gitlab.zoom.us/j/7952225338"><img align="left" src="https://img.shields.io/badge/Zoom-brightgreen?&style=for-the-badge&logo=Google Calendar&logoColor=white" /></a>
<a href="https://www.linkedin.com/in/lysanne-pinto-3a8a7a106/"><img align="left" src="https://img.shields.io/badge/LinkedIn-0A66C2?&style=for-the-badge&logo=LinkedIn&logoColor=white" /></a>
&nbsp;

## Development setup

- **Git client**: [GitKraken](https://www.gitkraken.com/)
- **Code editor**: [Visual Studio Code](https://code.visualstudio.com/) with extensions:
    - [Vale](https://marketplace.visualstudio.com/items?itemName=ChrisChinchilla.vale-vscode)
    - [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
    - [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
    - [Indent Rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow)
    - [Markdown Table Formatter](https://marketplace.visualstudio.com/items?itemName=fcrespo82.markdown-table-formatter)
    - [Path Autocomplete](https://marketplace.visualstudio.com/items?itemName=ionutvmi.path-autocomplete)
    - [Trailing Spaces](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces)
    - [VS Code Pets](https://marketplace.visualstudio.com/items?itemName=tonybaloney.vscode-pets)
- **Command line environment**:
    - [iTerm2](https://iterm2.com/)
    - [curl](https://curl.se/)
    - [Oh My Zsh](https://ohmyz.sh/)
    - [X-CMD](https://www.x-cmd.com/)
- **REST and GraphQL APIs**: [Hoppscotch](https://hoppscotch.com/)
- **Regular expressions**: [Regex101](https://regex101.com/) and [RegExr](https://regexr.com/)
- **Markdown notes**: [Bear](https://bear.app/)
